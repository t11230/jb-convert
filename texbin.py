from dataclasses import dataclass
from io import BufferedReader
from lzss import decompress
from pathlib import Path
from PIL import Image
from typing import List, Optional, Dict
import functools
import itertools
import struct


@dataclass
class DataEntry:
    data_size: int
    data_offset: int


@dataclass
class Texture:
    name: str
    data_size: int
    data_offset: int
    file_path: Path

    def extract_image(self) -> Optional[Image.Image]:
        with self.file_path.open('rb') as file:
            data = extract_data_entry(file, self.data_offset, self.data_size)
            return txdt_to_image(data)


def readcstr(f: BufferedReader) -> bytes:
    toeof = iter(functools.partial(f.read, 1), b'')
    return b''.join(itertools.takewhile(b'\0'.__ne__, toeof))


def parse_name_section(file: BufferedReader, section_offset: int) -> Dict[int, str]:
    file.seek(section_offset, 0)

    magic = file.read(4).decode('ascii')
    assert magic == 'PMAN'

    section_size = struct.unpack('<I', file.read(4))[0]
    unk_0 = struct.unpack('<Q', file.read(8))[0]
    file_count = struct.unpack('<I', file.read(4))[0]
    unk_1 = struct.unpack('<Q', file.read(8))[0]

    names: Dict[int, str] = {}

    for _ in range(file_count):
        _name_hash = struct.unpack('<I', file.read(4))[0]

        file_id = struct.unpack('<I', file.read(4))[0]

        name_offset = struct.unpack('<I', file.read(4))[0]
        last_pos = file.tell()

        file.seek(section_offset + name_offset, 0)
        file_name = readcstr(file).decode()

        names[file_id] = file_name

        file.seek(last_pos, 0)

    return names


def parse_data_entry_section(file: BufferedReader, section_offset: int, file_count: int) -> Dict[int, DataEntry]:
    file.seek(section_offset, 0)

    data_entries: Dict[int, DataEntry] = {}

    for idx in range(file_count):
        _unk_0 = struct.unpack('<I', file.read(4))[0]
        data_size = struct.unpack('<I', file.read(4))[0]
        data_offset = struct.unpack('<I', file.read(4))[0]

        data_entries[idx] = DataEntry(data_size, data_offset)

    return data_entries


def extract_data_entry(file: BufferedReader, offset: int, size: int) -> bytes:
    file.seek(offset, 0)

    decompressed_size = struct.unpack('>I', file.read(4))[0]
    compressed_size = struct.unpack('>I', file.read(4))[0]

    if compressed_size == 0:
        return file.read(decompressed_size)

    assert compressed_size == size - 8

    return decompress(file.read(compressed_size))


def txdt_to_image(data: bytes) -> Optional[Image.Image]:
    magic = data[:4].decode('ascii')
    if magic != 'TXDT' and magic != 'TDXT':
        print(f'Invalid TXDT magic: {magic}')
        return None

    width = struct.unpack('<H', data[0x10:0x12])[0]
    height = struct.unpack('<H', data[0x12:0x14])[0]

    data_format_value = data[0x14]

    data_format = {
        0x10: 'RGBA',
    }.get(data_format_value, None)

    if data_format is None:
        raise NotImplementedError(f'TXDT format {data_format} is not supported.')

    return Image.frombytes(data_format, (width, height), data[0x40:])


def parse_texbin_file(texbin_file: Path) -> List[Texture]:
    if not texbin_file.exists() or not texbin_file.is_file():
        return []

    with texbin_file.open('rb') as file:

        magic = file.read(4).decode('ascii')
        if magic != 'PXET':
            return []

        unk1 = struct.unpack('<I', file.read(4))[0]
        unk2 = struct.unpack('<I', file.read(4))[0]
        archive_size = struct.unpack('<I', file.read(4))[0]
        unk3 = struct.unpack('<I', file.read(4))[0]
        file_count = struct.unpack('<Q', file.read(8))[0]
        data_offset = struct.unpack('<I', file.read(4))[0]
        rect_offset = struct.unpack('<I', file.read(4))[0]
        unk4 = file.read(0x10)
        name_offset = struct.unpack('<I', file.read(4))[0]
        unk5 = struct.unpack('<I', file.read(4))[0]
        data_entry_offset = struct.unpack('<I', file.read(4))[0]

        if rect_offset != 0:
            print('This file contains rect data. This is unsupported.')

        if file_count == 0:
            print('This file doesn\'t contain any image data.')
            return []

        names = parse_name_section(file, name_offset)
        data_entries = parse_data_entry_section(file, data_entry_offset, file_count)

        textures: List[Texture] = []

        for idx, entry in data_entries.items():
            textures.append(Texture(
                name=names[idx],
                data_size=entry.data_size,
                data_offset=entry.data_offset,
                file_path=texbin_file
            ))

        return textures
