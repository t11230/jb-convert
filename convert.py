from decimal import Decimal
from ifstools import IFS
from jubeatools.formats import LOADERS, Format, DUMPERS
from jubeatools.song import Chart, Song, Metadata
from pathlib import Path
from pathvalidate import sanitize_filename
from tempfile import NamedTemporaryFile
from texbin import parse_texbin_file, Texture
from typing import Dict, Tuple, Iterator
import argparse
import binascii
import concurrent.futures
import csv
import ffmpeg
import shutil
import struct
import sys
import xml.etree.ElementTree as ET


def load_chart_levels(music_info_xml: Path) -> Dict[int, Dict[str, float]]:
    mappings: Dict[int, Dict[str, float]] = {}

    tree = ET.parse(music_info_xml)
    root = tree.getroot()

    for e in root.findall('./body/data'):
        music_id_elem = e.find('music_id')
        if music_id_elem is None or music_id_elem.text is None:
            continue

        song_id = int(music_id_elem.text)

        level_bsc_elem = e.find('level_bsc')
        if level_bsc_elem is not None and level_bsc_elem.text is not None:
            level_bsc = float(level_bsc_elem.text)
        else:
            level_bsc = 0

        level_adv_elem = e.find('level_adv')
        if level_adv_elem is not None and level_adv_elem.text is not None:
            level_adv = float(level_adv_elem.text)
        else:
            level_adv = 0

        level_ext_elem = e.find('level_ext')
        if level_ext_elem is not None and level_ext_elem.text is not None:
            level_ext = float(level_ext_elem.text)
        else:
            level_ext = 0

        mappings[song_id] = {
            'BSC': level_bsc,
            'ADV': level_adv,
            'EXT': level_ext,
        }

    return mappings

def load_stock_song_names(music_info_xml: Path) -> Dict[int, Tuple[str, str]]:
    names: Dict[int, Tuple[str, str]] = {}

    tree = ET.parse(music_info_xml)
    root = tree.getroot()

    for e in root.findall('./body/data'):
        music_id_elem = e.find('music_id')
        if music_id_elem is None or music_id_elem.text is None:
            continue

        song_id = int(music_id_elem.text)

        name_string_elem = e.find('name_string')
        if name_string_elem is None or name_string_elem.text is None:
            continue

        try:
            name = binascii.unhexlify(name_string_elem.text).decode('shift-jis')

        except binascii.Error:
            continue

        names[song_id] = (name, 'Unknown Artist')

    return names

def load_song_mappings(mapping_csv: Path) -> Dict[int, Tuple[str, str]]:
    mappings: Dict[int, Tuple[str, str]] = {}

    with mapping_csv.open('r', encoding='utf-8') as f:
        reader = csv.DictReader(f)
        for row in reader:
            mappings[int(row['songId'])] = (row['title'], row['artist'])

    return mappings

def get_manifest_files(manifest: ET.Element) -> Iterator[str]:
    for e in manifest.findall('./*'):
        if e.tag == "_info_":
            continue

        yield e.tag.replace('_E', '.').replace('__', '_')

def contains_song(components: Iterator[str]) -> bool:
    has_music = False
    has_charts = False

    for c in components:
        if c == 'bgm.bin':
            has_music = True
        elif 'eve' in c:
            has_charts = True

    return has_music and has_charts

def convert_music(input: Path, output: Path) -> None:
    with input.open('rb') as f:
        data = f.read(0x20)

        _magic = struct.unpack_from('>4s', data)[0]
        _data_size, _loop_start, _loop_end = struct.unpack_from('>III', data, 0x4)
        channels, bits = struct.unpack_from('<HH', data, 0x10)
        sample_rate, = struct.unpack_from('>I', data, 0x14)

        with NamedTemporaryFile() as tmp:
            tmp.write(f.read())
            tmp.flush()

            ffmpeg.run(
                ffmpeg
                .input(tmp.name, format='s16le', ac=channels, ar=sample_rate, codec='adpcm_ima_oki')
                .filter('highpass', f=10)
                .output(str(output), acodec='libvorbis', aq=3.0)
                .overwrite_output()
                .global_args("-loglevel", "quiet")
                .global_args("-nostats")
                .global_args("-hide_banner")
            )

def main() -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument('ifs', type=Path, help='Path to IFS directory')
    parser.add_argument('tex', type=Path, help='Path to tex directory')
    parser.add_argument('music_info', type=Path, help='Path to music info xml')
    parser.add_argument('out', type=Path, help='Path to output directory')
    parser.add_argument('-m', '--mapping', type=Path, default=Path(__file__).parent / 'song_registry.csv', help='Path to song mapping CSV')

    args = parser.parse_args()

    ifs_path: Path = args.ifs
    tex_path: Path = args.tex
    music_info_path: Path = args.music_info
    out_path: Path = args.out
    mapping_path: Path = args.mapping

    chart_levels = load_chart_levels(music_info_path)
    stock_names = load_stock_song_names(music_info_path)

    mappings = load_song_mappings(mapping_path)

    print("Loading textures")

    textures: Dict[str, Texture] = {}
    for p in tex_path.glob('**/*.bin'):
        for t in parse_texbin_file(p):
            textures[t.name] = t

    print("Extracting songs")

    if not out_path.exists():
        out_path.mkdir()

    pending_conversions = 0

    def convert_audio(input: Path, output: Path) -> None:
        nonlocal pending_conversions

        try:
            convert_music(input, output)
            input.unlink()
        except Exception as e:
            print(f'Failed to convert audio: {e}')

        pending_conversions -= 1

        print(f'Conversions remaining: {pending_conversions}')

    with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:

        for p in ifs_path.glob('**/*.ifs'):
            i = IFS(p)

            if i.is_file:
                files = get_manifest_files(i.manifest.xml_doc)
                if not contains_song(files):
                    continue

                ifs_id = p.stem.split('_')[0]

                print(f'Extracting song {ifs_id}')

                song_info = mappings.get(int(ifs_id))
                if song_info is None:
                    song_info = stock_names.get(int(ifs_id))

                    if song_info is None:
                        print(f'Unknown song id "{ifs_id}"')
                        continue

                    print(f'NOTE: Unknown song id "{ifs_id}", using name from music_info.xml')

                song_name, song_artist = song_info

                song_path = out_path / sanitize_filename(song_name)

                try:
                    i.extract(path=song_path, progress=False)

                    music_path = song_path / 'bgm.bin'
                    preview_path = song_path / 'idx.bin'

                    pending_conversions += 2
                    executor.submit(convert_audio, music_path, song_path / 'song.ogg')
                    executor.submit(convert_audio, preview_path, song_path / 'preview.ogg')

                    tex = textures.get(f'BNR_BIG_ID{ifs_id}')

                    if tex is not None:
                        print('Extracting banner')
                        img = tex.extract_image()
                        if img is not None:
                            img.save(song_path / 'banner.png')

                    if not (song_path / 'banner.png').exists():
                        print('Banner not found, using missing banner')
                        shutil.copyfile('missing.png', song_path / 'banner.png')

                    levels = chart_levels.get(int(ifs_id))

                    eve_loader = LOADERS[Format.EVE]

                    charts: Dict[str, Chart] = {}

                    for eve_file in song_path.glob('*.eve'):
                        tmp_song = eve_loader(eve_file)
                        for diff, ch in tmp_song.charts.items():
                            charts[diff] = ch
                            if levels and diff in levels:
                                ch.level = Decimal(levels[diff])
                        eve_file.unlink()

                    meta = Metadata(
                        title=song_name,
                        artist=song_artist,
                        audio=Path('song.ogg'),
                        cover=Path('banner.png'),
                        preview_file=Path('preview.ogg'),
                    )

                    song = Song(
                        metadata=meta,
                        charts=charts,
                        common_timing=None
                    )

                    output = DUMPERS[Format.MEMON_0_2_0](song, song_path)

                    for fname, contents in output.items():
                        with fname.open('wb') as f:
                            f.write(contents)

                except Exception as e:
                    print(f'Failed to extract song {song_name}: {e}')
                    shutil.rmtree(song_path)
                    continue

        print('Waiting for conversions to finish')

    print('Done')

    return 0

if __name__ == '__main__':
    sys.exit(main())
