# Inspired by: https://tim.cogan.dev/lzss/

from typing import Final

MATCH_LENGTH_MASK: Final[int] = 0xF
OFFSET_HIGH_MASK: Final[int] = 0xF0
WINDOW_SIZE: Final[int] = 4096
IS_MATCH_BIT: Final[bool] = True
LENGTH_OFFSET: Final[int] = 3

def decompress(compressed_bytes: bytes) -> bytes:
    compressed_offset = 0
    window_offset = 4078

    output_buffer = bytearray()
    window_buffer = bytearray(b'\x00'*WINDOW_SIZE)

    control_byte = 0
    control_bits_remaining = 0

    while True:
        if control_bits_remaining == 0:
            if compressed_offset >= len(compressed_bytes):
                break

            control_byte = compressed_bytes[compressed_offset]
            compressed_offset += 1
            control_bits_remaining = 8

        if control_byte & 0x01:
            data = compressed_bytes[compressed_offset]

            output_buffer.append(data)
            window_buffer[window_offset] = data

            compressed_offset += 1
            window_offset = (window_offset + 1) % WINDOW_SIZE

        else:
            if compressed_offset + 2 > len(compressed_bytes):
                break

            first_byte, second_byte = compressed_bytes[compressed_offset:compressed_offset+2]
            compressed_offset += 2

            offset = ((second_byte & OFFSET_HIGH_MASK) << 4) | first_byte
            length = (second_byte & MATCH_LENGTH_MASK) + LENGTH_OFFSET

            for _ in range(length):
                data = window_buffer[offset % WINDOW_SIZE]

                output_buffer.append(data)
                window_buffer[window_offset] = data

                window_offset = (window_offset + 1) % WINDOW_SIZE
                offset = (offset + 1) % WINDOW_SIZE

        control_byte >>= 1
        control_bits_remaining -= 1

    return bytes(output_buffer)
